import pyglet
import resources
import entities

game_window = pyglet.window.Window(
	width=800,
	height=650,
	caption="Execute After"
)

game_window.set_mouse_visible(False)

pyglet.gl.glClearColor(0, 0, 0, 1) #Clear the color buffer with color R,G,B,Alpha

player = entities.Entity(0, 0)
player.make_image(resources.enemy, player.x, player.y)

@game_window.event
def on_draw():
	game_window.clear()
	player.image.draw()

def update(dt):
	player.move_x_y(5,5)

if __name__ == '__main__':
	pyglet.clock.schedule_interval(update, 0.5)
	pyglet.app.run()
