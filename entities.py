import pyglet

class Entity(object):
    """ The base class for all tangible (has an x and y position) objects """
    def __init__(self, x=0, y=0):
        super(Entity, self).__init__()
        self.y = y
        self.x = x

    def make_image(self, *args, **kwargs):
        self.image = Image(*args, **kwargs);

    def move_x_y(self, dx=0, dy=0):
        self.x += dx
        self.y += dy

        if self.image != None:
            self.image.move_x_y(dx, dy)

class Image(pyglet.sprite.Sprite):
    """ Class to extend Entity to give it image or graphic functionality """
    def __init__(self, *args, **kwargs):
        super(Image, self).__init__(*args, **kwargs)

    def move_x_y(self, dx=0, dy=0):
        self.x += dx
        self.y += dy

class Collision(object):
    """docstring for Collision"""
    def __init__(self, arg):
        super(Collision, self).__init__()
        self.arg = arg  
